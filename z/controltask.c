/*
 * controltask.c
 *
 *  Created on: 10.12.2013
 *      Author: gruppeE6
 */

/* Control Task */
static void
control(long x)  // x wird nicht verwendet
{



  int b_cnt,cmd,timer = 0,minute=0,w_cnt;

  struct error err_msg;


  rt_printk("control: task started\n");

  /* Verbindung zum Modbus-Knoten herstellen */
  if ((c_no = rt_modbus_connect("modbus-node")) == -1)
    {
      rt_printk("control: task exited\n");
      return;
    }
  rt_printk("control: MODBUS communication opened\n");



  if(referenzfahrt() != 0){                             //Referenztask einmal ausf�hren und wenn ein Fehler auftritt, direkt wider beenden
      goto fail;
  }

  for (b_cnt = BandA; b_cnt < Bmax; b_cnt++)            // Band Tasks aufwecken
    rt_task_resume(&task_band[b_cnt]);

  for (b_cnt = BandA; b_cnt < Bmax; b_cnt++){            // Bänder entsprechend ihrer Laufrichtung anschalten
    cmd = bandrichtung[b_cnt][0];
    sendInt(&b_mbox[b_cnt], &cmd, b_cnt);

  }

  for (w_cnt = WeicheE; w_cnt < Wmax; w_cnt++)          // Nodetasks aufwecken
    rt_task_resume(&node_task[w_cnt]);

  /******************************/
  /*            ^               */
  /*            |               */
  /*            U               */
  /*(S) P----> (O) V ->         */
  /* ^                  L       */
  /* |                  |       */
  /* |                  |       */
  /* |                  v       */
  /* R             M-->(N)      */
  /*(K)                 Q       */
  /* ^                  |       */
  /* |                  |       */
  /* I                  v       */
  /*(G)<--------J------(T)      */
  /* ^                  H       */
  /* |                  |       */
  /* |                  v       */
  /* |           C---->(F)      */
  /* |                  D       */
  /* |                  |       */
  /* B                  v       */
  /*(E)<--------A------(W)      */
  /******************************/



  while (1)
    {
      /* Ablauf des Control Tasks*/


      if(rt_mbx_receive_timed(&controlbox,&err_msg,sizeof(error),10000 * nano2count(1000000)) == 0){ // 10 Sekunden auf eine Nachricht warten, wenn keine empfangen wurde, einfach weitermachen. Dient auch als Sleep ersatz
          rt_printk("Fehler! Quelle: ");                //Fehler entschl��sseln und ausgeben
          rt_printk(quellen[err_msg.quelle]);
          rt_printk(" %d " ,err_msg.nummer);
          rt_printk(errors[err_msg.grund]);
          goto fail;

      }


      timer++;
      rt_printk("Fehlerfreie Laufzeit: %d Min %d Sek\n",minute,timer*10);

      if(timer % 6 == 0){ //Sekunden und Minuten inkrementieren
          minute++;
          timer = 0;
      }

    }


  //Task normal beendet, deinitialisieren und beenden
  deinit();
  rt_modbus_disconnect(c_no);
  rt_printk("control: task exited normally\n");
  return;



  fail:

  //Task fehlerhaft beendet, deinitialisieren und beenden
  deinit();
  rt_printk("control: task exited with error(s)\n");
  rt_modbus_disconnect(c_no);

  return;


}
