/*
 * BandIO.h
 *
 *  Created on: 03.12.2013
 *      Author: gruppeE6
 */

#ifndef BANDIO_H_
#define BANDIO_H_


/* Ausgänge */
#define BandA_Vor       (unsigned long long)1<<0        /* DO.0 */
#define BandA_Zur       (unsigned long long)1<<1        /* DO.1 */
#define BandA_Speed     (unsigned long long)1<<0        /* AO.0 */

#define BandB_Vor       (unsigned long long)1<<2        /* DO.2 */
#define BandB_Zur       (unsigned long long)1<<3        /* DO.3 */
#define BandB_Speed     (unsigned long long)1<<1        /* AO.1 */

#define BandC_Vor       (unsigned long long)1<<4        /* DO.4 */
#define BandC_Zur       (unsigned long long)1<<5        /* DO.5 */
#define BandC_Speed     (unsigned long long)1<<2        /* AO.2 */

#define BandD_Vor       (unsigned long long)1<<6        /* DO.6 */
#define BandD_Zur       (unsigned long long)1<<7        /* DO.7 */
#define BandD_Speed     (unsigned long long)1<<3        /* AO.3 */

#define BandH_Vor       (unsigned long long)1<<20        /* DO.20 */
#define BandH_Zur       (unsigned long long)1<<21        /* DO.21 */
#define BandH_Speed     (unsigned long long)1<<4        /* AO.4 */

#define BandI_Vor       (unsigned long long)1<<22        /* DO.22 */
#define BandI_Zur       (unsigned long long)1<<23        /* DO.23 */
#define BandI_Speed     (unsigned long long)1<<5        /* AO.5 */

#define BandJ_Vor       (unsigned long long)1<<24        /* DO.24 */
#define BandJ_Zur       (unsigned long long)1<<25        /* DO.25 */
#define BandJ_Speed     (unsigned long long)1<<6        /* AO.6 */

#define BandL_Vor       (unsigned long long)1<<30        /* DO.30 */
#define BandL_Zur       (unsigned long long)1<<31        /* DO.31 */
#define BandL_Speed     (unsigned long long)1<<7        /* AO.7 */

#define BandM_Vor       (unsigned long long)1<<32        /* DO.32 */
#define BandM_Zur       (unsigned long long)1<<33        /* DO.33 */
#define BandM_Speed     (unsigned long long)1<<8        /* AO.8 */

#define BandP_Vor       (unsigned long long)1<<42        /* DO.42 */
#define BandP_Zur       (unsigned long long)1<<43        /* DO.43 */
#define BandP_Speed     (unsigned long long)1<<9        /* AO.9 */

#define BandQ_Vor       (unsigned long long)1<<44        /* DO.44 */
#define BandQ_Zur       (unsigned long long)1<<45        /* DO.45 */
#define BandQ_Speed     (unsigned long long)1<<10        /* AO.10 */

#define BandR_Vor       (unsigned long long)1<<46        /* DO.46 */
#define BandR_Zur       (unsigned long long)1<<47        /* DO.47 */
#define BandR_Speed     (unsigned long long)1<<11        /* AO.11 */

#define BandU_Vor       (unsigned long long)1<<56        /* DO.56 */
#define BandU_Zur       (unsigned long long)1<<57        /* DO.57 */
#define BandU_Speed     (unsigned long long)1<<12        /* AO.12 */

#define BandV_Vor       (unsigned long long)1<<58        /* DO.58 */
#define BandV_Zur       (unsigned long long)1<<59        /* DO.59 */
#define BandV_Speed     (unsigned long long)1<<13        /* AO.13 */


/* Eingänge */
#define BandA_IN_Front  (unsigned long long)1<<0        /* DI.0 */
#define BandA_IN_Back   (unsigned long long)1<<1        /* DI.1 */

#define BandB_IN_Front  (unsigned long long)1<<2        /* DI.2 */
#define BandB_IN_Back   (unsigned long long)1<<3        /* DI.3 */
#define BandB_IN_Center_Front  (unsigned long long)1<<65        /* DI.65 */ // Mittlerer Sensor
#define BandB_IN_Center_Back   (unsigned long long)1<<64        /* DI.64 */ // Mittlerer Sensor

#define BandC_IN_Front  (unsigned long long)1<<4        /* DI.4 */
#define BandC_IN_Back   (unsigned long long)1<<5        /* DI.5 */

#define BandD_IN_Front  (unsigned long long)1<<6        /* DI.6 */
#define BandD_IN_Back   (unsigned long long)1<<7        /* DI.7 */

#define BandH_IN_Front  (unsigned long long)1<<20        /* DI.20 */
#define BandH_IN_Back   (unsigned long long)1<<21        /* DI.21 */

#define BandI_IN_Front  (unsigned long long)1<<22        /* DI.22 */
#define BandI_IN_Back   (unsigned long long)1<<23        /* DI.23 */

#define BandJ_IN_Front  (unsigned long long)1<<24        /* DI.24 */
#define BandJ_IN_Back   (unsigned long long)1<<25        /* DI.25 */

#define BandL_IN_Front  (unsigned long long)1<<30        /* DI.30 */
#define BandL_IN_Back   (unsigned long long)1<<31        /* DI.31 */

#define BandM_IN_Front  (unsigned long long)1<<32        /* DI.32 */
#define BandM_IN_Back   (unsigned long long)1<<33        /* DI.33 */

#define BandP_IN_Front  (unsigned long long)1<<42        /* DI.42 */
#define BandP_IN_Back   (unsigned long long)1<<43        /* DI.43 */

#define BandQ_IN_Front  (unsigned long long)1<<44        /* DI.44 */
#define BandQ_IN_Back   (unsigned long long)1<<45        /* DI.45 */

#define BandR_IN_Front  (unsigned long long)1<<46        /* DI.46 */
#define BandR_IN_Back   (unsigned long long)1<<47        /* DI.47 */
#define BandR_IN_Center_Front  (unsigned long long)1<<67        /* DI.67 */ // Mittlerer Sensor (Bei den Verarbeitungsstationen)
#define BandR_IN_Center_Back   (unsigned long long)1<<66        /* DI.66 */ // Mittlerer Sensor (Bei den Verarbeitungsstationen)

#define BandU_IN_Front  (unsigned long long)1<<56        /* DI.56 */
#define BandU_IN_Back   (unsigned long long)1<<57        /* DI.57 */

#define BandV_IN_Front  (unsigned long long)1<<58        /* DI.58 */
#define BandV_IN_Back   (unsigned long long)1<<59        /* DI.59 */


#define FORW  3
#define BACKW 4


/* B�nder */
enum band_t
{

  BandA, BandB, BandC, BandD, BandH, BandI, BandJ, BandL, BandM, BandP, BandQ, BandR, BandU, BandV, Bmax

};

unsigned int  bandrichtung[Bmax][1] =
    {   // FORW  3
        // BACKW 4

        { FORW},
        { FORW },
        { FORW },
        { FORW },
        { FORW },
        { FORW },
        { FORW },
        { FORW },
        { FORW },
        { FORW },
        { FORW},
        { FORW },
        { BACKW },
        { FORW }
    };

/* Band MailBoxen*/
enum b_mbox_t
{

  BandAMbox, BandBMbox, BandCMbox, BandDMbox, BandHMbox, BandIMbox, BandJMbox, BandLMbox, BandMMbox, BandPMbox, BandQMbox, BandRMbox, BandUMbox, BandVMbox, BMback, BMmax
};

unsigned long long b_outputs[Bmax][3] =    //Offsets der B�nder
  {
    { BandA_Vor, BandA_Zur, BandA_Speed }, /*BandA*/
    { BandB_Vor, BandB_Zur, BandB_Speed }, /*BandB*/
    { BandC_Vor, BandC_Zur, BandC_Speed }, /*BandC*/
    { BandD_Vor, BandD_Zur, BandD_Speed }, /*BandD*/
    { BandH_Vor, BandH_Zur, BandH_Speed }, /*...*/
    { BandI_Vor, BandI_Zur, BandI_Speed },
    { BandJ_Vor, BandJ_Zur, BandJ_Speed },
    { BandL_Vor, BandL_Zur, BandL_Speed },
    { BandM_Vor, BandM_Zur, BandM_Speed },
    { BandP_Vor, BandP_Zur, BandP_Speed },
    { BandQ_Vor, BandQ_Zur, BandQ_Speed },
    { BandR_Vor, BandR_Zur, BandR_Speed },
    { BandU_Vor, BandU_Zur, BandU_Speed },
    { BandV_Vor, BandV_Zur, BandV_Speed }
  };

unsigned long long b_inputs[Bmax][2] =     //Sensoren der B�nder
  {
    { BandA_IN_Front, BandA_IN_Back },
    { BandB_IN_Front, BandB_IN_Back },
    { BandC_IN_Front, BandC_IN_Back },
    { BandD_IN_Front, BandD_IN_Back },
    { BandH_IN_Front, BandH_IN_Back },
    { BandI_IN_Front, BandI_IN_Back },
    { BandJ_IN_Front, BandJ_IN_Back },
    { BandL_IN_Front, BandL_IN_Back },
    { BandM_IN_Front, BandM_IN_Back },
    { BandP_IN_Front, BandP_IN_Back },
    { BandQ_IN_Front, BandQ_IN_Back },
    { BandR_IN_Front, BandR_IN_Back },
    { BandU_IN_Front, BandU_IN_Back },
    { BandV_IN_Front, BandV_IN_Back }
  };

#endif /* BANDIO_H_ */
