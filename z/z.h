/*
 * z.h
 *
 *  Created on: 10.12.2013
 *      Author: gruppeE6
 */

#ifndef Z_H_
#define Z_H_

#include "BandIO.h"
#include "WeicheIO.h"


#define BITSIZE 64              /* Bandsystem hat 64 Bit */
#define STACKSIZE 10000         /* reicht das? */

#define TIMEOUT 200             // TIMEOUT * BAUTEIL_WARTEZEIT
#define BAUTEIL_WARTEZEIT 10    // in ms
#define BAND_SPEED 30000        // Analoger Wert der Bandgeschwindigkeit

//Quellen der Fehler

#define QUELLE_WEICHE 0
#define QUELLE_BAND 1
#define QUELLE_TASK 2
#define QUELLE_REFERENZ 4
#define QUELLE_ANDERE 3

//Errorcodes

#define OK (int) 0
#define REFERENZ_ABGESCHMIERT 1
#define WEICHE_ABGESCHMIERT (int) 2
#define TASK_ABGESCHMIERT (int) 3


//Statische Variablen

//Semaphore
static SEM sem;


//Tasks
static RT_TASK task_band[Bmax];
static RT_TASK node_task[Wmax];
static RT_TASK task_control;

//Mailboxen
static MBX b_mbox[BMmax];
static MBX w_mbox[WMmax];
static MBX controlbox;

// Verbindungsnummer
static int c_no = 0;

// Array zum �bergeben der Kommandos
static char *command[] =
  { "P1", "P2", "P3", "FORWARD", "BACK", "STOP", "EXIT", "OK", "ERROR" };

// Fehlermeldungen
static char *errors[] =
  { "Alles OK", "Referenzfahrt abgeschmiert\n", "Weiche Abgeschmiert\n", "Task abgeschmiert\n" };

// Fehlerquellen
static char *quellen[] =
  { "Weiche", "Band", "Task", "Referenzfahrt", "Andere" };




//Error Message Struct
struct error{

  unsigned int quelle;
  unsigned int nummer;
  unsigned int grund;

}error;


// Modbus Kommandos
enum mask_t
{
  set, reset
};

// Kommandos der B�nder
enum command_t
{
  P1, P2, P3, FORWARD, BACK, STOP, EXIT };



#endif /* Z_H_ */
