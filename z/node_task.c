/*
 * weichetask.c
 *
 *  Created on: 10.12.2013
 *      Author: gruppeE6
 */

#include "z.h"
#include "node_task.h"
#include "WeicheIO.h"

/* Universeller Code fuer eine Weiche bzw. Node, da auch die Sensoren gesteuert werden  */

static void
node(long w_no)
{
  int timeout = 0;
  unsigned long long sensor_input;

  struct error err_msg;
  err_msg.quelle = QUELLE_WEICHE;
  err_msg.nummer = w_no;
  err_msg.grund = OK;

  rt_printk("weiche%d: task started\n", w_no);

  if (weiche_goto(w_no, band_sensor[w_no][7]) == -1)                    //Gehe zur Eingangspos. 1
    {
      err_msg.grund = WEICHE_ABGESCHMIERT;
      rt_mbx_send(&controlbox, &err_msg, sizeof(error));
      goto fail;
    }

  while (1)
    {

      rt_sleep(50 * nano2count(1000000));                               //Anderen auch etwas Zeit abgeben

      if (readAllBits(c_no, DIGITAL_IN, &sensor_input, BITSIZE))
        {       //Sensoren lesen
          goto fail;
        }

      if ((sensor_input & w_inputs[w_no][2]) != 0)                     //wenn schon bauteil in Weiche, erstmal abliefern
        {

          if (weiche_goto(w_no, band_sensor[w_no][6]) != 0)             //Gehe zur zielpos.
            {
              err_msg.grund = WEICHE_ABGESCHMIERT;
              rt_mbx_send(&controlbox, &err_msg, sizeof(error));
              goto fail;
            }

          if (bauteil_passiert_sensor(band_sensor[w_no][1], band_sensor[w_no][4], w_no) == -1)
            {  //warten bis bauteil drau�en ist
              goto fail;
            }

        }

      if (band_sensor[w_no][2] != 0) //B geh�rt unserer Gruppe bzw. ist ansprechbar?
        {

          if ((sensor_input & ((b_inputs[band_sensor[w_no][2]][band_sensor[w_no][5]]))) != 0 && (sensor_input & w_inputs[w_no][2]) == 0) //Sensor Eingang 2 was da && nichts in weiche drin?
            {

              if (weiche_goto(w_no, band_sensor[w_no][8]) == -1)                                     //gehe zu eingang 2
                {
                  err_msg.grund = WEICHE_ABGESCHMIERT;
                  rt_mbx_send(&controlbox, &err_msg, sizeof(error));
                  goto fail;
                }

              for (timeout = 0; timeout <= TIMEOUT; timeout++)                      //solange bauteil nicht drin, warten
                {
                  if (readAllBits(c_no, DIGITAL_IN, &sensor_input, BITSIZE))
                    {                           //Sensoren lesen
                      goto fail;
                    }

                  if ((sensor_input & w_inputs[w_no][2]) != 0)
                    {
                      break;
                    }
                  else
                    {
                      rt_sleep(BAUTEIL_WARTEZEIT * nano2count(1000000));

                    }

                }

              if (weiche_goto(w_no, band_sensor[w_no][6]) == -1)                                     //Gehe zur zielpos.
                {
                  err_msg.grund = WEICHE_ABGESCHMIERT;
                  rt_mbx_send(&controlbox, &err_msg, sizeof(error));
                  goto fail;
                }

              if (bauteil_passiert_sensor(band_sensor[w_no][1], band_sensor[w_no][4], w_no) == -1)
                {      //warten bis bauteil drau�en ist
                  goto fail;
                }

            }
          else if ((sensor_input & w_inputs[w_no][2]) != 0) //Sensor Eingang 2 nichts da aber schon Bauteil in Weiche
            {
              continue;
            }
        }

      if (readAllBits(c_no, DIGITAL_IN, &sensor_input, BITSIZE))
        {                                       //Sensoren lesen
          goto fail;
        }

      if ((sensor_input & ((b_inputs[band_sensor[w_no][2]][band_sensor[w_no][5]]))) != 0)     //Sensor Eingang 2 was da?
        {
          if (weiche_goto(w_no, band_sensor[w_no][8]) == -1)                                   //Gehe zur Eingangspos 2.
            {
              err_msg.grund = WEICHE_ABGESCHMIERT;
              rt_mbx_send(&controlbox, &err_msg, sizeof(error));
              goto fail;
            }
        }
      else
        {

          if (weiche_goto(w_no, band_sensor[w_no][7]) == -1)                                   //Gehe zur Eingangspos. 1
            {
              err_msg.grund = WEICHE_ABGESCHMIERT;
              rt_mbx_send(&controlbox, &err_msg, sizeof(error));
              goto fail;
            }

        }

    }

  rt_mbx_send(&controlbox, &err_msg, sizeof(error));
  rt_printk("weiche%d: task exited normal\n", w_no);
  return;

  fail:                                                                                            //Fehler aufgetreten

  err_msg.grund = TASK_ABGESCHMIERT;
  rt_mbx_send(&controlbox, &err_msg, sizeof(error));
  rt_printk("weiche%d: task aborted\n", w_no);
}
