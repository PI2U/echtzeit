/*
 * WeicheIO.h
 *
 *  Created on: 03.12.2013
 *      Author: gruppeE6
 */

#ifndef WEICHEIO_H_
#define WEICHEIO_H_

/* Weiche digitale Ausgaenge */
#define WeicheE_Ref     (unsigned long long)1<<8                /* DO.8  */
#define WeicheE_P1      (unsigned long long)1<<9                /* DO.9  */
#define WeicheE_P2      (unsigned long long)1<<10               /* DO.10 */
#define WeicheE_P3      (unsigned long long)1<<11               /* DO.11 */

#define WeicheF_Ref     (unsigned long long)1<<12               /* DO.12 */
#define WeicheF_P1      (unsigned long long)1<<13               /* DO.13 */
#define WeicheF_P2      (unsigned long long)1<<14               /* DO.14 */
#define WeicheF_P3      (unsigned long long)1<<15               /* DO.15 */

#define WeicheG_Ref     (unsigned long long)1<<16               /* DO.16 */
#define WeicheG_P1      (unsigned long long)1<<17               /* DO.17 */
#define WeicheG_P2      (unsigned long long)1<<18               /* DO.18 */
#define WeicheG_P3      (unsigned long long)1<<19               /* DO.19 */

#define WeicheK_Ref     (unsigned long long)1<<26               /* DO.26 */
#define WeicheK_P1      (unsigned long long)1<<27               /* DO.27 */
#define WeicheK_P2      (unsigned long long)1<<28               /* DO.28 */
#define WeicheK_P3      (unsigned long long)1<<29               /* DO.29 */

#define WeicheN_Ref     (unsigned long long)1<<34               /* DO.34 */
#define WeicheN_P1      (unsigned long long)1<<35               /* DO.35 */
#define WeicheN_P2      (unsigned long long)1<<36               /* DO.36 */
#define WeicheN_P3      (unsigned long long)1<<37               /* DO.37 */

#define WeicheO_Ref     (unsigned long long)1<<38               /* DO.38 */
#define WeicheO_P1      (unsigned long long)1<<39               /* DO.39 */
#define WeicheO_P2      (unsigned long long)1<<40               /* DO.40 */
#define WeicheO_P3      (unsigned long long)1<<41               /* DO.41 */

#define WeicheS_Ref     (unsigned long long)1<<48               /* DO.48 */
#define WeicheS_P1      (unsigned long long)1<<49               /* DO.49 */
#define WeicheS_P2      (unsigned long long)1<<50               /* DO.50 */
#define WeicheS_P3      (unsigned long long)1<<51               /* DO.51 */

#define WeicheT_Ref     (unsigned long long)1<<52               /* DO.52 */
#define WeicheT_P1      (unsigned long long)1<<53               /* DO.53 */
#define WeicheT_P2      (unsigned long long)1<<54               /* DO.54 */
#define WeicheT_P3      (unsigned long long)1<<55               /* DO.55 */

#define WeicheW_Ref     (unsigned long long)1<<60               /* DO.60 */
#define WeicheW_P1      (unsigned long long)1<<61               /* DO.61 */
#define WeicheW_P2      (unsigned long long)1<<62               /* DO.62 */
#define WeicheW_P3      (unsigned long long)1<<63               /* DO.63 */

/* Weiche digitale Eingaenge */
#define WeicheE_InPos   (unsigned long long)1<<8                /* DI.8  */
#define WeicheE_InMove  (unsigned long long)1<<9                /* DI.9  */
#define WeicheE_Teil    (unsigned long long)1<<10               /* DI.10 */
#define WeicheE_RefPos  (unsigned long long)1<<11               /* DI.11 */

#define WeicheF_InPos   (unsigned long long)1<<12               /* DI.12 */
#define WeicheF_InMove  (unsigned long long)1<<13               /* DI.13 */
#define WeicheF_Teil    (unsigned long long)1<<14               /* DI.14 */
#define WeicheF_RefPos  (unsigned long long)1<<15               /* DI.15 */

#define WeicheG_InPos   (unsigned long long)1<<16               /* DI.16 */
#define WeicheG_InMove  (unsigned long long)1<<17               /* DI.17 */
#define WeicheG_Teil    (unsigned long long)1<<18               /* DI.18 */
#define WeicheG_RefPos  (unsigned long long)1<<19               /* DI.19 */

#define WeicheK_InPos   (unsigned long long)1<<26               /* DI.26 */
#define WeicheK_InMove  (unsigned long long)1<<27               /* DI.27 */
#define WeicheK_Teil    (unsigned long long)1<<28               /* DI.28 */
#define WeicheK_RefPos  (unsigned long long)1<<29               /* DI.29 */

#define WeicheN_InPos   (unsigned long long)1<<34               /* DI.34 */
#define WeicheN_InMove  (unsigned long long)1<<35               /* DI.35 */
#define WeicheN_Teil    (unsigned long long)1<<36               /* DI.36 */
#define WeicheN_RefPos  (unsigned long long)1<<37               /* DI.37 */

#define WeicheO_InPos   (unsigned long long)1<<38               /* DI.38 */
#define WeicheO_InMove  (unsigned long long)1<<39               /* DI.39 */
#define WeicheO_Teil    (unsigned long long)1<<40               /* DI.40 */
#define WeicheO_RefPos  (unsigned long long)1<<41               /* DI.41 */

#define WeicheS_InPos   (unsigned long long)1<<48               /* DI.48 */
#define WeicheS_InMove  (unsigned long long)1<<49               /* DI.49 */
#define WeicheS_Teil    (unsigned long long)1<<50               /* DI.50 */
#define WeicheS_RefPos  (unsigned long long)1<<51               /* DI.51 */

#define WeicheT_InPos   (unsigned long long)1<<52               /* DI.52 */
#define WeicheT_InMove  (unsigned long long)1<<53               /* DI.53 */
#define WeicheT_Teil    (unsigned long long)1<<54               /* DI.54 */
#define WeicheT_RefPos  (unsigned long long)1<<55               /* DI.55 */

#define WeicheW_InPos   (unsigned long long)1<<60               /* DI.60 */
#define WeicheW_InMove  (unsigned long long)1<<61               /* DI.61 */
#define WeicheW_Teil    (unsigned long long)1<<62               /* DI.62 */
#define WeicheW_RefPos  (unsigned long long)1<<63               /* DI.63 */

enum w_mbox_t
{
  WeicheEMbox, WeicheFMbox, WeicheGMbox, WeicheKMbox, WeicheNMbox, WeicheOMbox, WeicheSMbox, WeicheTMbox, WeicheWMbox, WMback, WMmax

};

enum weiche_t
{
  WeicheE, WeicheF, WeicheG, WeicheK, WeicheN, WeicheO, WeicheS, WeicheT, WeicheW, Wmax

};

unsigned long long w_outputs[Wmax][4] =
  {
    { WeicheE_Ref, WeicheE_P1, WeicheE_P2, WeicheE_P3 },
    { WeicheF_Ref, WeicheF_P1, WeicheF_P2, WeicheF_P3 },
    { WeicheG_Ref, WeicheG_P1, WeicheG_P2, WeicheG_P3 },
    { WeicheK_Ref, WeicheK_P1, WeicheK_P2, WeicheK_P3 },
    { WeicheN_Ref, WeicheN_P1, WeicheN_P2, WeicheN_P3 },
    { WeicheO_Ref, WeicheO_P1, WeicheO_P2, WeicheO_P3 },
    { WeicheS_Ref, WeicheS_P1, WeicheS_P2, WeicheS_P3 },
    { WeicheT_Ref, WeicheT_P1, WeicheT_P2, WeicheT_P3 },
    { WeicheW_Ref, WeicheW_P1, WeicheW_P2, WeicheW_P3 }
    };

unsigned long long w_inputs[Wmax][4] =
  {
    { WeicheE_InPos, WeicheE_InMove, WeicheE_Teil, WeicheE_RefPos },
    { WeicheF_InPos, WeicheF_InMove, WeicheF_Teil, WeicheF_RefPos },
    { WeicheG_InPos, WeicheG_InMove, WeicheG_Teil, WeicheG_RefPos },
    { WeicheK_InPos, WeicheK_InMove, WeicheK_Teil, WeicheK_RefPos },
    { WeicheN_InPos, WeicheN_InMove, WeicheN_Teil, WeicheN_RefPos },
    { WeicheO_InPos, WeicheO_InMove, WeicheO_Teil, WeicheO_RefPos },
    { WeicheS_InPos, WeicheS_InMove, WeicheS_Teil, WeicheS_RefPos },
    { WeicheT_InPos, WeicheT_InMove, WeicheT_Teil, WeicheT_RefPos },
    { WeicheW_InPos, WeicheW_InMove, WeicheW_Teil, WeicheW_RefPos }
    };

//Wichtigstes Array um die Verbindung zwischen Weichen B�nder und Sensoren herzustellen
unsigned long long band_sensor[Wmax][9] =
  { //Ein 1, Ausg., Eing2,    ziel,   eing1,eing2(0=P1,1=P2,2=P3)               siehe Doku
    { BandA, BandB, 0 	 ,	1     , 0    ,-1   , 1 , 2	, 0  },	  	//Weiche E
    { BandH, BandD, BandC,	1     , 0    ,1    , 0 , 2	, 1  }, 	//Weiche F  , BandC und BandD vertauschen um Bohrmaschine anzusteuern
    { BandB, BandI, BandJ,      1     , 0    ,1    , 0 , 2	, 1  },		//Weiche G
    { BandI, BandR, 0    ,      1     , 0    ,-1   , 2 , 0	, 1  },		//Weiche K
    { BandL, BandQ, BandM,	1     , 0    ,1    , 0 , 2	, 1  },		//Weiche N ,  BandQ und BandM vertauschen um Bohrmaschine anzusteuern
    { BandP, BandV, BandU,	1     , 0    ,0    , 2 , 0	, 1  },		//Weiche O
    { BandR, BandP, 0 	 ,	1     , 0    ,-1   , 0 , 1	, 2  },		//Weiche S
    { BandQ, BandH, BandJ,	1     , 0    ,0    , 0 , 2	, 1  },		//Weiche T
    { BandD, BandA, 0	 ,	1     , 0    ,-1   , 1 , 2	, 0  }		//Weiche W
    };


#endif /* WEICHEIO_H_ */
