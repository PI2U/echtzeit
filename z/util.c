/*
 * util.c
 *
 *  Created on: 10.12.2013
 *      Author: gruppeE6
 */


/*
 * Funktion:  killB
 * --------------------
 * L�scht alle Band Tasks
 *
 *
 *  in: Band
 *  return:
 */

static void
killB(int b)
{
  int i = b;
  while (i-- > 0)
    rt_task_delete(&task_band[i]);
}

/*
 * Funktion:  killW
 * --------------------
 * L�scht alle Node Tasks
 *
 *
 *  in: Weiche
 *  return:
 */

static void
killW(int w)
{
  int i = w;
  while (i-- > 0)
    rt_task_delete(&node_task[i]);
}


/*
 * Funktion:  readAllBits
 * --------------------
 * Funktion um Modbus Knoten auszulesen
 *
 *
 * in: Verbindungsnummer, Quelle(DIGITAL_IN etc.), Ausgabepointer, L�nge
 * return: wenn error, dann -1 sonst 0
 *
 * Die Transportsysteme arbeiten 64 Bit breit... (32 Bit fuer MODBUS-GUI)
 */

static int
readAllBits(int c_no, int from, unsigned long long *to, int length)
{

  int offset;
  unsigned short val = 0;

  *to = 0;
  for (offset = length / 16 - 1; offset >= 0; offset--)
    {
      if (rt_modbus_get(c_no, from, offset, &val))
        return -1;
      *to |= (unsigned long long) val << (offset * 16);
    }


  return 0;
}

/*
 * Funktion:  writeAllBits
 * --------------------
 * Funktion um Modbus Knoten zu beschreiben
 *
 *
 * in: Verbindungsnummer, Ziel(DIGITAL_OUT etc.), neuer Wert, L�nge
 * return: wenn error, dann -1 sonst 0
 *
 */
static int
writeAllBits(int c_no, int to, unsigned long long from, int length)
{
  int offset;
  unsigned short val;

  for (offset = 0; offset <= length / 16 - 1; offset++)
    {
      val = from >> (offset * 16);
      if (rt_modbus_set(c_no, to, offset, val))
        return -1;
    }
  return 0;
}

/*
 * Funktion:  maskAllBits
 * --------------------
 * Funktion um Modbus Knoten zu maskieren
 *
 *
 * in: Verbindungsnummer, Ziel(DIGITAL_OUT etc.), Maske, Set oder Reset, L�nge
 * return: wenn error, dann -1 sonst 0
 *
 */
static int
maskAllBits(int c_no, int iotype, unsigned long long mask, int masktype, int length)
{

  unsigned long long val;

  if (rt_sem_wait(&sem) == 0xffff)
    {
      rt_printk("Fehler bei Semaphore \n");
      return -1;
    }


  if (readAllBits(c_no, iotype, &val, length) == -1)
    {
      return -1;
    }


  if (masktype == set)
    val |= mask;
  else if (masktype == reset)
    val &= ~mask;


  if (writeAllBits(c_no, iotype, val, length) == -1)
    {
      rt_printk("Fehler bei writeAllBits \n");
      return -1;
    }

  /* Weiteres Problem: Wird nach dem Schreiben der Ausgaenge der Zustand der
   * Ausgaenge eher wieder eingelesen, als die Ausgaenge physikalisch aktiv
   * sind, werden falsche Werte gelesen. Deshalb muss an dieser Stelle
   * gewartet werden, bis die Ausgaenge stabil sind. Wie lange? Hmm, die
   * Modbus-Doku sagt nix, also ausprobieren!
   */

  rt_sleep(2 * nano2count(1000000));

  if (rt_sem_signal(&sem) == 0xffff)
    {
      rt_printk("Fehler bei Semaphore \n");
      return -1;
    }

  return 0;
}


/*
 * Funktion:  sendInt
 * --------------------
 * Senden eines Integers per Mailbox
 *
 *
 * in: Ziel Mailbox, pointer auf Kommando, Band-/Weichennummer etc.
 * return:
 *
 */
static void
sendInt(MBX *mbx, int *cmd, int to)
{
  rt_mbx_send(mbx, cmd, sizeof(int));
}


/*
 * Funktion:  deinit
 * --------------------
 * Funktion um alle B�nder und Nodes zu stoppen
 *
 *
 * in:
 * return:
 *
 */
static void deinit(void){
  int cmd,b_cnt;

  cmd = EXIT;

  for (b_cnt = BandA; b_cnt < Bmax; b_cnt++){   //Stoppe B�nder
    sendInt(&b_mbox[b_cnt], &cmd, b_cnt);
  }
  rt_sleep(1000 * nano2count(1000000));         //Warte sicherheitshalber
  killW(Wmax);                                  //Stoppe Weichen(Nodes)

}

/*
 * Funktion:  referenzfahrt
 * --------------------
 * Referenzfahrt aller Weichen
 *
 *
 * in:
 * return: 0, im Fehlerfall -1
 *
 */
static int
referenzfahrt(void)
{
  unsigned long long result;
  int timeout = 0;
  struct error err_msg;
  err_msg.quelle = QUELLE_REFERENZ;
  err_msg.nummer = 0;
  err_msg.grund = OK;

  rt_printk("Referenzfahrt\n");

  /* Ausgaenge initialisieren */
  if (writeAllBits(c_no, DIGITAL_OUT, 0, BITSIZE))
    goto fail;
  rt_sleep(100 * nano2count(1000000));

  /* Referenzfahrt einschalten */
  if (writeAllBits(c_no, DIGITAL_OUT,
      WeicheE_Ref | WeicheF_Ref | WeicheG_Ref | WeicheK_Ref | WeicheN_Ref | WeicheO_Ref | WeicheS_Ref | WeicheT_Ref | WeicheW_Ref,
      BITSIZE))
    goto fail;

  /* Warten bis in Position */
  do
    {
      rt_sleep(100 * nano2count(1000000));
      if (readAllBits(c_no, DIGITAL_IN, &result, BITSIZE) || timeout == 100){ //10 Sek warten
        goto fail;
      }
      timeout++;
    }
  while ((result & (WeicheE_InPos | WeicheF_InPos | WeicheG_InPos | WeicheK_InPos | WeicheN_InPos | WeicheO_InPos | WeicheS_InPos | WeicheT_InPos | WeicheW_InPos)) != (WeicheE_InPos | WeicheF_InPos | WeicheG_InPos | WeicheK_InPos | WeicheN_InPos | WeicheO_InPos | WeicheS_InPos | WeicheT_InPos | WeicheW_InPos));

  /* Referenzfahrt ausschalten */
  if (writeAllBits(c_no, DIGITAL_OUT, 0, BITSIZE))
    goto fail;

  rt_printk("referenz: position reached\n");




  return 0;

  fail:
  err_msg.grund = REFERENZ_ABGESCHMIERT;                //Fehlermeldung setzen falls etwas schief gelaufen ist
  rt_mbx_send(&controlbox,&err_msg,sizeof(error));

  return -1;
}


/*
 * Funktion:  bauteil_passiert_sensor
 * --------------------
 * Funktion um zu pr�fen ob ein Bauteil einen Sensor passiert hat. Timings sind hier wichtig, damit der Sensor das Bauteil nicht �bersieht
 *
 *
 * in: Band, Sensor des Bandes, Dazugeh�rige Weiche
 * return: 0, im Fehlerfall -1
 *
 */
static int
bauteil_passiert_sensor(int band, int sensor, int weiche )
{

  unsigned long long sensor_in;

  if( readAllBits(c_no, DIGITAL_IN, &sensor_in, BITSIZE) == -1){
      return -1;
  }

  while ((sensor_in & w_inputs[weiche][2]) != 0)
      {
        if( readAllBits(c_no, DIGITAL_IN, &sensor_in, BITSIZE) == -1){
            return -1;
        }
        rt_sleep(50 * nano2count(1000000));
      }


  rt_sleep(300 * nano2count(1000000));

  if( readAllBits(c_no, DIGITAL_IN, &sensor_in, BITSIZE) == -1){
      return -1;
  }

  while ((sensor_in & b_inputs[band][sensor]) != 0)
      {
        if( readAllBits(c_no, DIGITAL_IN, &sensor_in, BITSIZE) == -1){
          return -1;
        }
        rt_sleep(50 * nano2count(1000000));

      }


  return 0;
}

/*
 * Funktion:  weiche_goto
 * --------------------
 * Funktion um eine Position mit einer Weiche anzusteuern
 *
 *
 * in: Weiche, Zielposition
 * return: 0, im Fehlerfall -1
 *
 */
static int
weiche_goto(int weiche, int pos)
{
  int timeout = 0;
  unsigned long long result;


  /* Position setzen */
  if (pos == P1)
    {

      if (maskAllBits(c_no, DIGITAL_OUT, w_outputs[weiche][1], set, BITSIZE))
        {

          goto fail;
        }
    }
  else if (pos == P2)
    {

      if (maskAllBits(c_no, DIGITAL_OUT, w_outputs[weiche][2], set, BITSIZE))
        {

          goto fail;
        }
    }
  else if (pos == P3)
    {

      if (maskAllBits(c_no, DIGITAL_OUT, w_outputs[weiche][3], set, BITSIZE))
        {

          goto fail;
        }
    }

  do
    {
      if(timeout == 25){
          goto fail;
      }

      rt_sleep(100 * nano2count(1000000));

      if (readAllBits(c_no, DIGITAL_IN, &result, BITSIZE))
        goto fail;

      timeout++;

    }
  while ((result & w_inputs[weiche][0]) != w_inputs[weiche][0]);        // Warten bis Weiche in Position, wenn Timeout erreicht gehe zur Fehlerbehandlung


  /* Position ruecksetzen */
  if (pos == P1)
    {
      if (maskAllBits(c_no, DIGITAL_OUT, w_outputs[weiche][1], reset, BITSIZE))
        goto fail;
    }
  else if (pos == P2)
    {
      if (maskAllBits(c_no, DIGITAL_OUT, w_outputs[weiche][2], reset, BITSIZE))
        goto fail;
    }
  else if (pos == P3)
    {
      if (maskAllBits(c_no, DIGITAL_OUT, w_outputs[weiche][3], reset, BITSIZE))
        goto fail;
    }


  return 0;
  fail:
  return -1;
}
