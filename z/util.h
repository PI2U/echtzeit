/*
 * util.h
 *
 *  Created on: 10.12.2013
 *      Author: gruppeE6
 */

#ifndef UTIL_H_
#define UTIL_H_


#include "util.c"

/* Prototypen START */
static int
maskAllBits(int c_no, int iotype, unsigned long long mask, int masktype,
    int length);

static int
writeAllBits(int c_no, int to, unsigned long long from, int length);

static int
readAllBits(int c_no, int from, unsigned long long *to, int length);

static void
sendInt(MBX *mbx, int *cmd, int to);

static int referenzfahrt(void);

static void deinit(void);


static void
killB(int b);

static void
killW(int w);

static int bauteil_passiert_sensor(int band, int sensor, int weiche);

/* Prototypen ENDE */

#endif /* UTIL_H_ */
