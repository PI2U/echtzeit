/*
 * bandtask.c
 *
 *  Created on: 10.12.2013
 *      Author: gruppeE6
 */

/* Universeller Code fuer alle B�nder; b_no spezifiziert das Ziel */

static void
band(long b_no)
{
  int cmd;

  struct error err_msg;
  err_msg.quelle = QUELLE_BAND;
  err_msg.nummer = b_no;
  err_msg.grund = OK;

  rt_printk("band%d: task started\n", b_no);

  /* Main Loop */
  while (1)
    {
      /* Auf Auftrag warten */
      rt_mbx_receive(&b_mbox[b_no], &cmd, sizeof(int));

      rt_printk("band %d: received %s from control\n", b_no, command[cmd]);

      /* Band vorw�rts */
      if (cmd == FORWARD)
        {

          maskAllBits(c_no, DIGITAL_OUT, b_outputs[b_no][1], reset, BITSIZE);        //Erstmal das jeweils andere Bit auf 0, da das Band sonst steht wenn vorw�rts und r�ckw�rts gleichzeitig 1 sind
          rt_modbus_set(c_no, IB_IL_AO, b_no, BAND_SPEED);                           //Analogen Ausgang f�r die Geschwindigkeit setzen

          if (maskAllBits(c_no, DIGITAL_OUT, b_outputs[b_no][0], set, BITSIZE))      //Band anschalten
            {
              goto fail;                                                             //Fehler?
            }

        }
      else if (cmd == BACK)                                                          //Genau wie oben
        {

          maskAllBits(c_no, DIGITAL_OUT, b_outputs[b_no][0], reset, BITSIZE);

          rt_modbus_set(c_no, IB_IL_AO, b_no, BAND_SPEED);

          if (maskAllBits(c_no, DIGITAL_OUT, b_outputs[b_no][1], set, BITSIZE))
            {
              goto fail;
            }

        }
      else if (cmd == STOP)                                                          //Beim Stop werden alle Bits auf 0 gesetzt
        {

          rt_modbus_set(c_no, IB_IL_AO, b_no, (unsigned short) 0);

          if (maskAllBits(c_no, DIGITAL_OUT, b_outputs[b_no][0] | b_outputs[b_no][1], reset, BITSIZE))
            {
              goto fail;
            }
        }
      else if (cmd == EXIT)                                                         //Wenn Exit empfangen wurde, wird wie bei Stop vorgegangen nur wird anschlie�end die Schleife beendet
        {

          rt_modbus_set(c_no, IB_IL_AO, b_no, (unsigned short) 0);
          if (maskAllBits(c_no, DIGITAL_OUT, b_outputs[b_no][0] | b_outputs[b_no][1], reset, BITSIZE))
            {
              goto fail;
            }

          break;                                                                    //Schleife beenden

        }

    }

  rt_mbx_send(&controlbox, &err_msg, sizeof(error));                                //Wenn kein Fehler aufgetreten ist, wird OK an den Controltask gesendet
  rt_printk("Band %d: task exited normal\n", b_no);
  return;

  fail:

  err_msg.grund = TASK_ABGESCHMIERT;                                                //Wenn ein Fehler aufgetreten ist, wird der entsprechende Fehlercode an den Controltask gesendet
  rt_mbx_send(&controlbox, &err_msg, sizeof(error));
  rt_printk("Band %d: task aborted\n", b_no);
}
