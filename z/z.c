/*
 * Beispiel: Grundgeruest einer Modbus-Kommunikation
 *
 * Aufbau einer Modbus-Kommunikation ohne weitere Funktion
 *
 */

#include <linux/module.h>
#include <rtai_mbx.h>
#include <rtai_sched.h>

//#include "rtai_modbus.h"
#include <sys/rtai_modbus.h>

#include "z.h"
#include "util.h"
#include "bandtask.h"
#include "controltask.h"
#include "node_task.h"

MODULE_LICENSE("GPL");







/* Funktion die beim Entfernen des Moduls aufgerufen wird */
static void __exit
example_exit(void)
{
  int b_cnt;
  int w_cnt;
  int cmd;

  stop_rt_timer();




  rt_printk("Stoppe Bänder\n");

  cmd = EXIT;

  for (b_cnt = BandA; b_cnt < Bmax; b_cnt++){
    sendInt(&b_mbox[b_cnt], &cmd, b_cnt);               // Exit an alle B�nder senden
  }

  rt_sleep(500 * nano2count(1000000));                  //sicherheitshalber warten bis alle B�nder gestoppt sind

  for (b_cnt = BandA; b_cnt < Bmax; b_cnt++){
    rt_task_delete(&task_band[b_cnt]);                  //Bandtasks l�schen
  }

  rt_printk("B�nder gestoppt\n");

  for (w_cnt = WeicheE; w_cnt < Wmax; w_cnt++)
      rt_task_delete(&node_task[w_cnt]);                //Nodetasks l�schen


  rt_task_delete(&task_control);                        //Controltask l�schen

  for (b_cnt = BandAMbox; b_cnt < BMmax; b_cnt++)
    rt_mbx_delete(&b_mbox[b_cnt]);                      //band Mailboxen l�schen

  for (w_cnt = WeicheEMbox; w_cnt < WMmax; w_cnt++)
      rt_mbx_delete(&w_mbox[w_cnt]);                    //Weichen Mailboxen l�schen

  rt_mbx_delete(&controlbox);                           //Control Mailbox und Semaphore l�schen
  rt_sem_delete(&sem);


  printk("rtai_example unloaded\n");
}

/* Funktion die beim Laden des Moduls aufgerufen wird */
static int __init
example_init(void)
{
  int b_cnt;
  int w_cnt;
  int b_box_cnt = 0;
  int w_box_cnt = 0;

  /* variables Timing basierend auf dem CPU-Takt */
  rt_set_oneshot_mode();

  /* Timer starten */
  start_rt_timer(0);

  /* Semaphore initialisieren */
  rt_typed_sem_init(&sem, 1, CNT_SEM);


  /* Modbuskommunikation initialisieren */
  modbus_init();

  /* Mail-Boxen f�r die Baender initialisieren */
  for (b_box_cnt = BandAMbox; b_box_cnt < BMback; b_box_cnt++)
  {
    if (rt_mbx_init(&b_mbox[b_box_cnt], sizeof(int)))
      {
        printk("Band mailbox %d konnte nicht initialisiert werden!\n", b_box_cnt);
        goto fail;
      }
  }

  /* Mail-Box f�r den Controltask initialisieren */
  if (rt_mbx_init(&controlbox, sizeof(error)))
    {
      printk("Control mailbox konnte nicht initialisiert werden!\n");
      goto fail;
    }

  /* Mail-Boxen f�r die Weichen initialisieren */
    for (w_box_cnt = WeicheEMbox; w_box_cnt < WMback; w_box_cnt++)
    {
      if (rt_mbx_init(&w_mbox[w_box_cnt], sizeof(int)))
        {
          printk("Weiche mailbox %d konnte nicht initialisiert werden!\n", w_box_cnt);
          goto fail;
        }
    }

  /* Mail-Box f�r die Antwort der B�nder initialisieren */
  if (rt_mbx_init(&b_mbox[BMback], Bmax * sizeof(int)))
    {
      printk("Konnte die Antwort Mail-Box für die Bänder nicht initialisieren %d\n", BMback);
      goto fail;
    }

  /* Mail-Box f�r die Antwort der Nodes initialisieren */
  if (rt_mbx_init(&w_mbox[WMback], Wmax * sizeof(int)))
     {
       printk("Konnte die Antwort Mail-Box für die Weichen nicht initialisieren %d\n", WMback);
       goto fail;
     }

  /* Taskinitialisierung
   *
   * &task = Adresse des zu initialisierenden TCB
   * control = zum Task gehörende Funktion
   * 0 = Uebergabeparameter an die zum Task gehoerende Funktion (long)
   * 1024 = Stacksize
   * 0 = Priorität des Tasks (0 = groesste)
   * 0 = uses_fpu (Fliesskommaeinheit nicht benutzen); im Praktikum sollte es 0 bleiben
   * NULL = &signal_handler; Referenz zu einer Fkt., die bei jeder Aktivierung
   * des Tasks aufgerufen wird, ansonsten NULL
   *
   * Achtung: Nach der Initialisierung ist der Task "suspended"!
   *
   */
  if (rt_task_init(&task_control, control, 0, STACKSIZE, 10, 0, NULL))
    {
      printk("cannot initialize control task\n");
      goto fail;
    }


  /* Bänder initialisieren */
  for (b_cnt = BandA; b_cnt < Bmax; b_cnt++){



      if (rt_task_init(&task_band[b_cnt], band, b_cnt, STACKSIZE, 10, 0, NULL))
      {
        printk("cannot initialize band task %d\n", b_cnt);

        killB(b_cnt);           //richtig Aufr�umen
        goto fail;
      }

  }
  /* Nodes initialisieren */
  for (w_cnt = WeicheE; w_cnt < Wmax; w_cnt++)
    if (rt_task_init(&node_task[w_cnt], node, w_cnt, STACKSIZE, 10, 0, NULL))
      {
        printk("cannot initialize Node task %d\n", w_cnt);

        killW(w_cnt);           //richtig Aufr�umen
        killB(b_cnt);
        goto fail;
      }

  /* suspend -> ready bzw. running */
  rt_task_resume(&task_control);
  printk("rtai_example loaded\n");
  return (0);

  fail:

  stop_rt_timer();
  return (1);
}

/* Modulein- und ausstieg festlegen */
module_exit(example_exit)
module_init(example_init)










